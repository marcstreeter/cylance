# Global Unique Identifier API

## Project Overview
Here is the file structure layout from the root of this project
```commandline
johnsmith@Johns-MBP:~/cylance$ tree .
.
├── README.md
├── api
│   ├── Dockerfile
│   ├── requirements.txt
│   └── srv
│       ├── main.py
│       ├── models
│       │   ├── __init__.py
│       │   └── guid.py
│       ├── utils
│       │   ├── __init__.py
│       │   ├── errors
│       │   │   ├── __init__.py
│       │   │   ├── base.py
│       │   │   ├── system.py
│       │   │   └── user.py
│       │   └── setup.py
│       └── views
│           ├── __init__.py
│           ├── base.py
│           ├── error.py
│           └── guid.py
├── db
│   ├── Dockerfile
│   └── init.sql
└── docker-compose.yml
```
## Local Testing

#### Running Local Docker Environment

This code can be tested using Docker containerization locally - [docker installation guide is here](https://docs.docker.com/engine/installation/).

This setup requires two separate docker containers: one for the API and one for the database.  Both are managed via a `docker-compose.yml` file.  To start them enter the following from the root of the project.

```commandline
johnsmith@Johns-MBP:~/cylance$ docker-compose up
```

it will proceed to install all dependencies and then start up both containers.

#### Running Local computer

May also run on local computer by first installing all dependencies (hopefully in your own virtual environment)

```commandline
johnsmith@Johns-MBP:~/cylance$ cd api
johnsmith@Johns-MBP:~/cylance/api$ conda create -n cylance python=3
johnsmith@Johns-MBP:~/cylance/api$ source activate cylance
(cylance)johnsmith@Johns-MBP:~/cylance/api$ pip install -r requirements.txt
```

and then you may run it by supplying required environment variables

```commandline
(cylance)johnsmith@Johns-MBP:~/cylance/api$ cd srv
(cylance)johnsmith@Johns-MBP:~/cylance/api/srv$ python main.py <DATABASE_USER> <DATABASE_PASS> <DATABASE_HOST> <DATABASE_NAME>
```
or get help for more options
```commandline
(cylance)johnsmith@Johns-MBP:~/cylance/api/srv$ python main.py --help
```

#### Step 2: Testing/Developing
While the docker image is running you can make requests to it locally 

```commandline
curl -X "POST" "http://127.0.0.1:8888/guid/" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d $'{
  "expire": "1427736345",
  "user": "Cylance, Inc."
}'
```

## TODO

Here are a couple topics that came up while I was working on this.

  - come to consensus on use of PUT and POST with respect to updating
  - take into consideration any enviroments that don't have the PUT HTTP verb
  - confirm desired structure of error output
  - confirm desired structure of OK output. For example, we only provide a dictionary of our 
object's key's however we don't provide any details about the request (transaction code). 
Also our structure would deviate when multiple objects are returned(ie, implementing a `GET` all guid's).  
We might consider always returning an object that describes the interaction with a result 
field that is always a list regardless of whether we're pulling one or multiple results
  - confirm that `204` is acceptable status code for delete
  - confirm that `201` is acceptable status code for create


## API commands specification
### Create
Creates a new GUID and stores it in the database along with the metadata provided. If a GUID is not specified, the API will generate a random one.

_EXAMPLE INPUT_

    curl -X "POST" "http://127.0.0.1:8888/guid/" \
         -H "Content-Type: application/json; charset=utf-8" \
         -d $'{
      "expire": "1427736345",
      "user": "Cylance, Inc."
    }'
_OUTPUT_

    HTTP/1.1 201 Created
    Server: TornadoServer/4.5.1
    Content-Type: application/json; charset=UTF-8
    Date: Fri, 25 Aug 2017 03:07:38 GMT
    Content-Length: 93
    
    {"guid": "5784773C6CD84A338318ADC009A39250", "expire": "1427736345", "user": "Cylance, Inc."}


_EXAMPLE INPUT_

    curl -X "POST" "http://127.0.0.1:8888/guid/" \
         -H "Content-Type: application/json; charset=utf-8" \
         -d $'{
      "user": "Cylance, Inc."
    }'

_OUTPUT_

    {
        "guid": "9094E4C980C74043A4B586B420E69DDF",
        "expire": "1427736345",
        "user": "Cylance, Inc."
    }


### Read
Returns the metadata associated to the given GUID.

_EXAMPLE INPUT_

    curl "http://127.0.0.1:8888/guid/A604C0BE88314D6AB1342005DBA29E88"

_OUTPUT_

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.1
    Content-Type: application/json; charset=UTF-8
    Date: Fri, 25 Aug 2017 03:14:08 GMT
    Etag: "6e356e79709a1060c2f877f872b94bf14efe5c95"
    Content-Length: 93
    
    {"guid": "A604C0BE88314D6AB1342005DBA29E88", "expire": "1506222733", "user": "Cylance, Inc."}


### Update
Updates the metadata associated to the given GUID. The GUID itself cannot be updated using this command.

_EXAMPLE INPUT_

    curl -X "POST" "http://127.0.0.1:8888/guid/A604C0BE88314D6AB1342005DBA29E88" \
         -H "Content-Type: application/json; charset=utf-8" \
         -d $'{
      "expire": "1427822745"
    }'
    
_OUTPUT_

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.1
    Content-Type: application/json; charset=UTF-8
    Date: Fri, 25 Aug 2017 03:16:46 GMT
    Content-Length: 93
    
    {"guid": "A604C0BE88314D6AB1342005DBA29E88", "expire": "1427822745", "user": "Cylance, Inc."}


### Delete
Deletes the GUID and its associated data.

_EXAMPLE INPUT_

    curl -X "DELETE" "http://127.0.0.1:8888/guid/A604C0BE88314D6AB1342005DBA29E88" \
     -H "Content-Type: application/json; charset=utf-8" \
     -d $'{}'

_OUTPUT_

    HTTP/1.1 204 No Content
    Server: TornadoServer/4.5.1
    Date: Fri, 25 Aug 2017 03:17:56 GMT
    
## API error codes
The following response codes should be returned by the service: 
 
 - 200's on accepted/successful requests
 - 400's on client errors
 - 500's on server errors
     