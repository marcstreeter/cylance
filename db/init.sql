-- QUICK AND DIRTY DATABASE, no thought put into naming!
CREATE TABLE example(
    id BIGSERIAL PRIMARY KEY,
    guid character varying(32) UNIQUE NOT NULL,
    name character varying(255) NOT NULL,
    expire bigint NOT NULL,
    active BOOLEAN DEFAULT TRUE
);
