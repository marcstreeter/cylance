import argparse
import asyncio
import logging

from tornado.web import Application
from tornado.platform.asyncio import AsyncIOMainLoop
from tornado.httpserver import HTTPServer
from tornado.options import define

from views import GUIDView
from views import NotFoundView
from utils import setup

logger = setup.log(version='v1', name='guid')
logging.getLogger('queries').setLevel(logging.ERROR)
logging.getLogger('tornado').setLevel(logging.ERROR)

parser = argparse.ArgumentParser(description='cylance product integration engineer applicant programming test')
parser.add_argument('dbuser', help='database username string')
parser.add_argument('dbpass', help='database password string')
parser.add_argument('dbhost', help='database host string (ex: 120.0.0.1)')
parser.add_argument('dbname', help='database name string')
parser.add_argument('--dbport', help='database port integer (ex: 5432)', type=int, default=5432)
parser.add_argument('--port', help='API server port integer (ex: 8080)', type=int, default=8080)
config = parser.parse_args()


# input these into tornado's global options
# note: I would have just used tornado's options but ¯\_(ツ)_/¯
define('username', default=config.dbuser, type=str, group='db')
define('password', default=config.dbpass, type=str, group='db')
define('host', default=config.dbhost, type=str, group='db')
define('name', default=config.dbname, type=str, group='db')
define('port', default=config.dbport, type=int, group='db')


def main():
    logger.info("starting tornado server..")
    AsyncIOMainLoop().install()
    server = HTTPServer(
        Application(
            [
                (r"/guid/([0-9A-F]{32})", GUIDView),
                (r"/guid/", GUIDView),
            ],
            default_handler_class=NotFoundView
        )
    )
    server.bind(config.port)
    server.start()
    logger.info("server is waiting for a connection...")
    asyncio.get_event_loop().run_forever().start()

if __name__ == '__main__':
    main()
