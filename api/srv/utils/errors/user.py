from .base import CylanceErrors


class UserErrors(CylanceErrors):
    _err_categroy = "USER"
    _err_message = "user error"
    _err_reason = "user error"
    _err_code = 400


class InvalidRequest(UserErrors):
    _err_message = "invalid request"


class ItemNotFound(UserErrors):
    _err_message = "item not found"
    _err_code = 404


class ItemCreateFailed(UserErrors):
    _err_message = "item failed to create"
    _err_code = 409


class ItemUpdateFailed(UserErrors):
    _err_message = "item failed to update"
    _err_code = 409


class ItemDestroyFailed(UserErrors):
    _err_message = "item failed deletion"
    _err_code = 409

