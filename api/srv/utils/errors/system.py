from .base import CylanceErrors


class SystemErrors(CylanceErrors):
    _err_category = "SYSTEM"
    _err_code = 500
    _err_message = "system error"
    _err_reason = "system error"
