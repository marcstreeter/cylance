
import logging
from logging import StreamHandler
import sys

logger = logging.getLogger(__name__)


def log(name, version, **options):
    """
    log configuration setup, logging all goes to standard out by default
    :param str name: endpoint name
    :param str version: endpoint version
    :param options: TODO add options for other notification devices (smtp, slack)
    :return:
    """
    # DO NOT 'log' anything here, logging is being set up so infinite loops happen
    log_format = '{asctime} [{process}] [{app_name}|{app_vers}] [{levelname}] {filename}:{lineno} - {message}'

    def filter_factory():
        class LoggingFilter(logging.Filter):
            def filter(self, record):
                # TODO add additional record props
                try:
                    record.app_name = name
                except:
                    record.app_name = '<name>'

                try:
                    record.app_vers = version
                except:
                    record.app_vers = '<vers>'
                return True
        return LoggingFilter

    formatter = logging.Formatter(log_format, style="{")
    LogFilter = filter_factory()
    new_logger = logging.root
    new_logger.setLevel(logging.DEBUG)

    # standard out
    log_handler_s = StreamHandler(sys.stdout)
    log_handler_s.setLevel(logging.DEBUG)
    log_handler_s.setFormatter(formatter)
    log_handler_s.addFilter(LogFilter())
    new_logger.addHandler(log_handler_s)

    return new_logger