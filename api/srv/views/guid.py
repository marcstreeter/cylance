import logging

from tornado.web import HTTPError
from tornado.escape import json_decode
from voluptuous import (
    All,
    Length,
    Match,
    Optional,
    Required,
    Schema
)

from models import guid as model
from views import BaseView
from utils import errors

# schema fields
user_msg = 'user field must be between 1-255 characters in length'
user_field = Schema(All(str, Length(min=1, max=255, msg=user_msg)))
guid_msg = 'guid field must be a string of 32 hexacimal values'
guid_field = Schema(All(str, Match(r"^[0-9A-F]{32}$", msg=guid_msg)))
expire_msg = 'expire field must be a string of integers'
expire_field = Schema(All(str, Match(r"^[0-9]+$", msg=expire_msg)))

# input schema
create_schema = Schema({
    Required('user'): user_field,
    Optional('guid'): guid_field,
    Optional('expire'): expire_field
})
update_schema = Schema({
    Optional('user'): user_field,
    Optional('expire'): expire_field
})

logger = logging.getLogger(__name__)


class GUIDView(BaseView):
    async def post(self, guid=None):
        """
        create new guid

        **Example request**:
        POST /guid HTTP/1.1
        Content-Length: 29
        Host: localhost:5555
        Content-Type: application/json; charset=UTF-8
        {
            "expire": "1427736345",
            "user": "Cylance, Inc."
        }

        **Example response**:
        HTTP/1.1 201 Created
        Server: TornadoServer/4.5.1
        Content-Type: application/json; charset=UTF-8
        Date: Thu, 24 Aug 2017 20:55:34 GMT
        Content-Length: 93

        {"guid": "1A9FA24E14F14CD2B4A708A62D4C7F88", "expire": "1427736345", "user": "Cylance, Inc."}
        :statuscode 201: no error, resource created
        :statuscode 409: failed to create resource
        :statuscode 500: system error
        """
        if guid:
            # TODO come to consensus on use of PUT and POST wrt updating
            # TODO review enviroments that don't have the PUT HTTP verb
            await self.put(guid=guid)
        else:
            # validate input
            try:
                kwargs = create_schema(json_decode(self.request.body))
            except Exception as e:
                raise HTTPError(400, reason=str(e))

            # execute
            try:
                result = await model.create(session=self.session, **kwargs)
            except Exception as e:
                raise HTTPError(500, f"failed to create model({e})")

            # prepare output
            try:
                ordered_keys = ('guid', 'expire', 'user')
                output = self.prepare_output(output=result, ordered_keys=ordered_keys)
            except Exception as e:
                raise HTTPError(500, f"failed to prepare create output({e})")

            # send output
            try:
                self.set_status(201)
                self.write(output)
                self.finish()
            except Exception as e:
                raise HTTPError(500, f"failed to send create output({e})")

    async def get(self, guid):
        """
        retrieve guid details

        **Example request**:
        GET /guid/1A9FA24E14F14CD2B4A708A62D4C7F88 HTTP/1.1
        Host: 127.0.0.1:8888
        Connection: close
        User-Agent: Paw/3.1.3 (Macintosh; OS X/10.12.6) GCDHTTPRequest

        **Example response**:
        HTTP/1.1 200 OK
        Server: TornadoServer/4.5.1
        Content-Type: application/json; charset=UTF-8
        Date: Thu, 24 Aug 2017 21:27:32 GMT
        Etag: "60206aa5d167884d548afa7c73fb208f428e43df"
        Content-Length: 93

        {"guid": "1A9FA24E14F14CD2B4A708A62D4C7F88", "expire": "1427736345", "user": "Cylance, Inc."}

        """
        logger.debug("entered get")
        # execute
        try:
            result = await model.read(session=self.session, guid=guid)
        except errors.CylanceErrors:
            raise
        except Exception as e:
            raise HTTPError(500, f"failed to read model({e})")
        else:
            logger.debug("read model")

        # prepare output
        try:
            ordered_keys = ('guid', 'expire', 'user')
            output = self.prepare_output(output=result, ordered_keys=ordered_keys)
        except Exception as e:
            raise HTTPError(500, f"failed to prepare read ouput({e})")
        else:
            logger.debug("read output prepared")

        # send output
        try:
            self.write(output)
            self.finish()
        except Exception as e:
            raise HTTPError(500, f"failed to send read output({e})")

    async def put(self, guid):
        """
        update guid

        **Example request**:
        POST /guid/1A9FA24E14F14CD2B4A708A62D4C7F88 HTTP/1.1
        Content-Type: application/json; charset=utf-8
        Host: 127.0.0.1:8888
        Connection: close
        User-Agent: Paw/3.1.3 (Macintosh; OS X/10.12.6) GCDHTTPRequest
        Content-Length: 23

        {"expire":"1427822745"}

        **Example response**:
        HTTP/1.1 200 OK
        Server: TornadoServer/4.5.1
        Content-Type: application/json; charset=UTF-8
        Date: Thu, 24 Aug 2017 21:55:50 GMT
        Content-Length: 93

        {"guid": "1A9FA24E14F14CD2B4A708A62D4C7F88", "expire": "1427822745", "user": "Cylance, Inc."}
        :statuscode 200: resource updated
        :statuscode 409: failed to update resource
        :statuscode 500: system error
        """
        # OK OK, I know you didn't ask for this, but I'm supplying this here because generally PUT is used for updates
        # and POST is only used for creates (unless you're using some old mechanism that doesn't understand PUT)
        logger.debug("entered put")
        # validate input
        try:
            kwargs = update_schema(json_decode(self.request.body))
        except Exception as e:
            raise HTTPError(400, reason=str(e))

        # execute
        try:
            result = await model.update(session=self.session, guid=guid, **kwargs)
        except errors.CylanceErrors:
            raise
        except Exception as e:
            raise HTTPError(500, f"failed to update model({e})")
        else:
            logger.debug("read model")

        # prepare output
        try:
            ordered_keys = ('guid', 'expire', 'user')
            output = self.prepare_output(output=result, ordered_keys=ordered_keys)
        except Exception as e:
            raise HTTPError(500, f"failed to prepare update output({e})")
        else:
            logger.debug("update output prepared")

        # send output
        try:
            self.write(output)
            self.finish()
        except Exception as e:
            raise HTTPError(500, f"failed to send update output({e})")

    async def delete(self, guid):
        """
        delete guid details

        **Example request**:
        DELETE /guid/1A9FA24E14F14CD2B4A708A62D4C7F88 HTTP/1.1
        Content-Type: application/json; charset=utf-8
        Host: 127.0.0.1:8888
        Connection: close
        User-Agent: Paw/3.1.3 (Macintosh; OS X/10.12.6) GCDHTTPRequest
        Content-Length: 2

        {}

        **Example response**:
        HTTP/1.1 204 No Content
        Server: TornadoServer/4.5.1
        Date: Thu, 24 Aug 2017 21:59:41 GMT
        """
        logger.debug("entered put")
        # execute

        try:
            await model.destroy(session=self.session, guid=guid)
        except errors.CylanceErrors:
            raise
        except Exception as e:
            raise HTTPError(500, f"failed to delete model({e})")
        else:
            logger.debug("read model")

        # send output
        try:
            self.set_status(204)
            self.finish()
        except Exception as e:
            raise HTTPError(500, f"failed to send delete output({e})")







