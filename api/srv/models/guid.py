import logging
import time
import uuid

from tornado.gen import convert_yielded
import queries

from utils import errors

logger = logging.getLogger(__name__)

# TODO establish config value for this
default_expiration = (30 * 24 * 3600)  # 30 days in seconds

async def create(session, user: str, guid:str=None, expire:str=None) -> dict:
    """
    create new guid entry
    """
    if guid:
        new_guid = guid
    else:
        new_guid = str(uuid.uuid4()).replace('-', '').upper()

    # TODO timezone aware expire and synchronization
    if expire:
        new_expire = int(expire)
    else:
        new_expire = int(time.time() + default_expiration)

    sql = (
        "INSERT INTO example (guid, name, expire) "
        f"VALUES (%(guid)s, %(name)s, %(expire)s);"
    )
    params = {
        'guid': new_guid,
        'name': user,
        'expire': new_expire
    }

    try:
        results = await convert_yielded(
            session.query(sql, parameters=params)
        )
        results.free()
    except (queries.DataError, queries.IntegrityError):
        raise errors.ItemCreateFailed("failed to create guid")
    except Exception as e:
        raise errors.SystemErrors(message=f"failed to send query({e})")
    else:
        # TODO establish central location for serialized output of guid rows (avoid copy pasta)
        return {
            'guid': str(params['guid']),
            'user': str(params['name']),
            'expire': str(params['expire'])
        }


async def read(session, guid: str) -> dict:
    """
    reads any *active* guid entry
    """
    logger.debug(f"reading '{guid}'")
    sql = (
        "SELECT * FROM example where guid = %(guid)s AND active = TRUE"
    )
    params = {
        'guid': guid
    }

    results = await convert_yielded(
        session.query(sql, parameters=params)
    )

    try:
        data = results.as_dict()
        count = results.count()
        results.free()
        if count != 1:
            raise errors.ItemNotFound(reason="no guid found")
    except:
        raise
    else:
        return {
            'guid': str(data['guid']),
            'user': str(data['name']),
            'expire': str(data['expire'])
        }
    finally:
        results.free()


async def update(session, guid, user=None, expire=None) -> dict:
    """
    update guid entry fields (user, expire)
    """
    logger.debug(f"updating '{guid}'")
    params = {
        'name': user,
        'expire': expire,
        'guid': guid
    }
    params = {k:v for k,v in params.items() if v is not None}
    clause = " ".join(
        f"{key}=%({key})s"
        for key, value in params.items()
        if key != 'guid'
    )
    # TODO confirm behavior of inability to update guid's that are inactive
    sql = (
        f"UPDATE example SET {clause} WHERE guid=%(guid)s AND active=TRUE"
    )
    logger.debug(f"SENDING '{sql}' with {params}")
    results = await convert_yielded(
        session.query(sql, parameters=params)
    )

    if results.count() != 1:
        results.free()
        raise errors.ItemUpdateFailed()

    sql = (
        f"SELECT * FROM example WHERE guid=%(guid)s"
    )
    results = await convert_yielded(
        session.query(sql, parameters=params)
    )

    try:
        data = results.as_dict()
    except:
        raise errors.ItemUpdateFailed()
    else:
        return {
            'guid': str(data['guid']),
            'user': str(data['name']),
            'expire': str(data['expire'])
        }
    finally:
        results.free()


async def destroy(session, guid:str) -> None:
    """
    deactivates a guid entry
    NOTE: guid's `deleted` field is updated, data persists
    NOTE: difference between updating and deletion is that
          deletion allows repetitive deletion with the same
          output, whereas updates can differ depending on if
          a record went from active to inactive
    """
    logger.debug(f"destroying '{guid}'")
    params = {
        'guid': guid
    }
    sql = (
        f"UPDATE example SET active=FALSE WHERE guid=%(guid)s"
    )
    results = await convert_yielded(
        session.query(sql, parameters=params)
    )
    has_results = results.count() > 0
    results.free()

    if has_results:
        return
    else:
        raise errors.ItemNotFound("failed to find guid for deletion")